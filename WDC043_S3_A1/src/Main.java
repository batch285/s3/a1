import java.util.Scanner;
import java.util.InputMismatchException;

public class Main {
    public static void main(String[] args) {

        //loop version
        System.out.println("Enter an integer to compute its factorial:");

        Scanner in = new Scanner(System.in);
        int num = 0;


        try {
            num = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter an integer value.");
            return;
        }

        if (num < 0) {
            System.out.println("Factorial cannot be computed for negative numbers.");
            return;
        }

        int answer = 1;
        int counter = 1;

        while (counter <= num) {
            answer *= counter;
            counter++;
        }

        System.out.println("The factorial of " + num + " is " + answer);


        //While version
        /*
        *         System.out.println("Input an integer whose factorial will be computed");

        Scanner in = new Scanner(System.in);
        int num = 0;

        try {
            num = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a valid integer");
            return;
        }

        if (num < 0) {
            System.out.println("Factorial cannot be computed for negative numbers.");
            return;
        }

        int answer = 1;
        int counter = 1;

        while (counter <= num) {
            answer *= counter;
            counter++;
        }

        System.out.println("The factorial of " + num + " is " + answer);


        * */

    }
}
